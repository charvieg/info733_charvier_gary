package CharvierGary;

import java.util.ArrayList;
import java.util.List;

public class Systeme {
    private Carte carte;

    private List<Table> listeTables = new ArrayList<Table> ();

    public List<Commande> commandesArchivees = new ArrayList<Commande> ();
    
    public List<Ticket> tickets=new ArrayList<Ticket> ();
    
    public Systeme(){
    	this.carte = new Carte();
    }

    public void afficherCarte() {
    }

    public List<Table> recupTablesDipo() {
    	List<Table> tableDispo = new ArrayList<Table> ();
    	for(Table T : listeTables){
    		if(T.esTuLibre()) tableDispo.add(T);
    	}
    	return tableDispo;
    }
    
    public float montantCommande(int numeroTable){
    	for(Table T : listeTables){
    		if (T.getNumero() == numeroTable) return T.montantCommande();	
    	}
    	return 0;
    }
    
    public void commandeReglee(int numeroTable){
    	Commande archive = null; // pour stocker la commande qui sera à archiver
    	for(Table T : listeTables){
    		if (T.getNumero() == numeroTable) {
    			archive = new Commande(T.commandeReglee());
    			T.liberation();
    			break;
    		}
    		    		
    	}
    	this.commandesArchivees.add(archive);
    }
    
    public void envoyerTicket(int numeroTable, boolean premierTicket){
    	Ticket temp = null;
    	for(Table T : listeTables){
    		if (T.getNumero() == numeroTable) {
    			temp = T.envoyerTicket(premierTicket);
    			break;
    		}
    	}
    	if (temp != null) this.tickets.add(temp);
    }
    
    public List<ElementMenu> prochainTicket(int numeroTable)
    {
    	return this.tickets.get(0).afficherComposition();
	}
    
    public void traitementTicketTermine()
    {
    	this.tickets.get(0).annoterTicket();
    	this.tickets.remove(0);
    }
    
    public List<ElementMenu> prochainTicket() {
    	return this.tickets.get(0).afficherComposition();
    }
    
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */

    
	public Carte getCarte() {
		return carte;
	}

	public void setCarte(Carte carte) {
		this.carte = carte;
	}

	public List<Table> getListeTables() {
		return listeTables;
	}

	public void setListeTables(List<Table> listeTables) {
		this.listeTables = listeTables;
	}

	public boolean listeTicketVide() {
		return (this.tickets.isEmpty());
	}

}
