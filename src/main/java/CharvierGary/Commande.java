package CharvierGary;

import java.util.ArrayList;
import java.util.List;

public class Commande {
    private List<MenuDeCommande> menu = new ArrayList<MenuDeCommande> ();

    private List<ElementMenu> elementMenu = new ArrayList<ElementMenu> ();

    private List<Ticket> tickets = new ArrayList<Ticket> ();
    
    private EtatCommande etat;
    
    public Commande(){
    	this.etat = new CommandeAttenteChoix(this);
    }
    
    public Commande(Commande commande) {
		this.menu = commande.getMenu();
		this.elementMenu = commande.getElementMenu();
		this.tickets = commande.getTickets();
		this.etat = commande.getEtat();


	}

	public void ajouterElement(List<ElementMenu> element){
    	getEtat().ajouterElement(element);
    }


    public void ajouterMenu(List<MenuDeCarte> tmpMenu) {
    	getEtat().ajouterMenu(tmpMenu);
    }
    

    public List<MenuDeCommande> recupMenus() {
    	return getEtat().recupMenus();
    }
    
   
    
    public List<ElementMenu> elementsRestant(){
    	return getEtat().elementsRestants();
	}

    
    
    public void creerTicket(List<ElementMenu> listeElementsTicket) {
    	getEtat().creerTicket(listeElementsTicket);
    }
    
    public void repasFini() {
    	getEtat().repasFini();
    }

    public float montantCommande() {
    	return getEtat().montantCommande();
    }

    public Commande commandeReglee() {
    	return getEtat().commandeReglee();
    }
    
    public boolean resteTicket(){
    	return(this.getTickets().size() != 0);
    }
    
	public Ticket envoyerTicket(boolean premierTicket) {
		etat.envoyerTicket(premierTicket);
		if (this.tickets.size() != 0) return this.tickets.get(0);
		else return null;
		
	}
	
	public void choixTermine(){
		this.etat.choixTermine();
	}
	
	public void creationTerminee(){
		this.etat.creationTerminee();
	}
    
    
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */

	public List<ElementMenu> getElementMenu() {
		return elementMenu;
	}
	
	

	public void setElementMenu(List<ElementMenu> elementMenu) {
		this.elementMenu = elementMenu;
	}

	public List<MenuDeCommande> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuDeCommande> menu) {
		this.menu = menu;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public Class<? extends EtatCommande> etatDeCommande(){
		return getEtat().getClass();
	}

	public EtatCommande getEtat() {
		return etat;
	}

	public void setEtat(EtatCommande etat) {
		this.etat = etat;
	}



}
