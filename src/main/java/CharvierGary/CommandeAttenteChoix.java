package CharvierGary;

import java.util.List;


public class CommandeAttenteChoix implements EtatCommande {
	private Commande commande;
	
	public CommandeAttenteChoix(Commande commande){
		this.commande = commande;
	}


	public void ajouterElement(List<ElementMenu> element) {
		commande.getElementMenu().addAll(element);
	}


	public void creerTicket(List<ElementMenu> listeElementsTicket) {
		throw new IllegalStateException("Impossible de créer des tickets sur une commande" +
				" en attente de choix!");
	}


	public List<MenuDeCommande> recupMenus() {
		return this.commande.getMenu();
	}


	public List<ElementMenu> elementsRestants() {
		throw new IllegalStateException("Le client passe sa commande !");
	}


	public void ajouterMenu(List<MenuDeCarte> menu) {
    	for (MenuDeCarte M : menu){
    		commande.getMenu().add(new MenuDeCommande(M));
    	}
	}


	public void repasFini() {
		throw new IllegalStateException("Le client passe sa commande !");
		
	}


	public float montantCommande() {
		throw new IllegalStateException("Le client passe sa commande !");
	}


	public Commande commandeReglee() {
		throw new IllegalStateException("La commandes est pas encore passée, elle " +
				"elle ne peut pas déjà être réglée !");
		
	}
	
	public void choixTermine(){
		this.commande.setEtat(new CommandeAttenteCretionTicket(commande));
	}


	@Override
	public void envoyerTicket(boolean premierTicket) {
		throw new IllegalStateException("Le client esten train de choisir ce qu'il veut manger ...");	
		
	}


	@Override
	public void creationTerminee() {
		throw new IllegalStateException("Le client esten train de choisir ce qu'il veut manger ...");	
		
	}
}
