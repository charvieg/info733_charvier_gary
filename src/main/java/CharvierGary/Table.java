package CharvierGary;

import java.util.List;

public class Table {
    private int Numero;

    private boolean EstLibre = true;

    private int NbPlaces;

    private Commande commandeCourante;
    
    public Table(int numero, int nbPlaces){
    	this.setNumero(numero);
    	this.setNbPlaces(nbPlaces);
    }

    public boolean esTuLibre() {
    	return EstLibre;
    }

    public void attibuerTable() {
    	this.EstLibre = false;
    	setCommandeCourante(new Commande());
    }

    public void ajouterElement(List<ElementMenu> element) {
    	getCommandeCourante().ajouterElement(element);
    }

    public void ajouterMenu(List<MenuDeCarte> menu) {
    	getCommandeCourante().ajouterMenu(menu);
    }

    public List<MenuDeCommande> recupMenus() {
    	return this.getCommandeCourante().recupMenus();
    }

    public Commande commandeReglee(){
    	return commandeCourante.commandeReglee();
    }
    

	public float montantCommande() {
		return this.commandeCourante.montantCommande();
	}
	
	public void liberation(){
		this.EstLibre = true;
		this.commandeCourante = null;
	}
	
	public Ticket envoyerTicket(boolean premierTicket) {
		return this.commandeCourante.envoyerTicket(premierTicket);
		
	}
	
	
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */
    
	public int getNbPlaces() {
		return NbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		NbPlaces = nbPlaces;
	}

	public int getNumero() {
		return Numero;
	}

	public void setNumero(int numero) {
		Numero = numero;
	}

	public Commande getCommandeCourante() {
		return commandeCourante;
	}

	public void setCommandeCourante(Commande commandeCourante) {
		this.commandeCourante = commandeCourante;
	}




}
