package CharvierGary;

public class TicketEnvoye implements EtatTicket {
	private Ticket ticket;
	
	public TicketEnvoye(Ticket ticket)
	{
		this.ticket = ticket;
	}
	
	@Override
	public void annoterTicket() {
		ticket.setEtat(new TicketEnPreparation(ticket));
	}

}
