package CharvierGary;

import java.util.ArrayList;
import java.util.List;

public class Carte {
    private List<MenuDeCarte> listeMenus = new ArrayList<MenuDeCarte> ();

    private ListeElementMenu listeElementMenu = new ListeElementMenu();
    
    public Carte(){
    	
    }
    
    public MenuDeCarte chercheMenu(int idMenu){
    	for(MenuDeCarte M : this.getListeMenus()){
    		if (M.esTuMenu(idMenu)) return M;
    	}
		return null;
    }
    
    public List afficherSelection(String selection) {

		// Retourner la liste des boissons
		if (selection == "boisson"){
    		return this.getListeElementMenu().getListeBoissons();
    	}
		
		// Retourner la liste des entrées
		if (selection == "entree"){
    		return this.getListeElementMenu().getListeEntrees();
    	}
		
		// Retourner la liste des plats
		if (selection == "plat"){
    		return this.getListeElementMenu().getListePlats();
    	}
		
		// Retourner la liste des desserts
		if (selection == "dessert"){
    		return this.getListeElementMenu().getListeDesserts();
    	}
		
		if (selection == "menu"){
			return this.getListeMenus();
		}
		return null;
    }
    
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */

	public List<MenuDeCarte> getListeMenus() {
		return listeMenus;
	}

	public void setListeMenus(List<MenuDeCarte> listeMenus) {
		this.listeMenus = listeMenus;
	}

	public ListeElementMenu getListeElementMenu() {
		return listeElementMenu;
	}

	public void setListeElementMenu(ListeElementMenu listeElementMenu) {
		this.listeElementMenu = listeElementMenu;
	}

}
