package CharvierGary;

import java.util.List;

public class Menu {
    private String Nom;

    private float Prix;

    private int IdMenu;
    
    public Menu(String nom, float prix, int idMenu){
    	this.Nom = nom;
    	this.Prix = prix;
    	this.IdMenu = idMenu;
    }

    public List<ElementMenu> recupSelection(String type) {
    	return null;
    }

    
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */
    
    
	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public float getPrix() {
		return Prix;
	}

	public void setPrix(float prix) {
		Prix = prix;
	}

	public int getIdMenu() {
		return IdMenu;
	}

	public void setIdMenu(int idMenu) {
		IdMenu = idMenu;
	}

}
