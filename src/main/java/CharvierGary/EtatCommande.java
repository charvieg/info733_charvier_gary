package CharvierGary;

import java.util.List;

public interface EtatCommande {

    public void ajouterElement(List<ElementMenu> element);

    public void creerTicket(List<ElementMenu> listeElementsTicket);

    public List<MenuDeCommande> recupMenus();
    
    public void choixTermine();

    public List<ElementMenu> elementsRestants();

    public void ajouterMenu(List<MenuDeCarte> menu);

    public void repasFini();

    public float montantCommande();

    public Commande commandeReglee();
    
    public void creationTerminee();
    
    public void envoyerTicket(boolean premierTicket);

}
