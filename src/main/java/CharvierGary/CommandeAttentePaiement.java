package CharvierGary;

import java.util.List;


public class CommandeAttentePaiement implements EtatCommande {

	private Commande commande;
	
	public CommandeAttentePaiement(Commande commande) {
		this.commande = commande;
	}

	public void ajouterElement(List<ElementMenu> element) {
		throw new IllegalStateException("Le client a fini de manger!");		
	}

	public void creerTicket(List<ElementMenu> listeElementsTicket) {
		throw new IllegalStateException("Le client a fini de manger!");	
		
	}

	public List<MenuDeCommande> recupMenus() {
		throw new IllegalStateException("Le client a fini de manger!");	
	}

	public List<ElementMenu> elementsRestants() {
		throw new IllegalStateException("Le client a fini de manger!");	
	}

	public void ajouterMenu(List<MenuDeCarte> menu) {
		throw new IllegalStateException("Le client a fini de manger!");	
	}

	public void repasFini() {
		throw new IllegalStateException("Le client a fini de manger!");	
	}

	public float montantCommande() {
		float temp = 0f;
		
		for(ElementMenu M : commande.getElementMenu()){
			temp += M.getPrix();
		}
		for(MenuDeCommande M : commande.getMenu()){
			temp += M.getPrix();
		}
		
		return temp;
	}

	public Commande commandeReglee() {
		this.commande.setEtat(new CommandeTerminee(commande));
		return commande;
	}
	
	public void choixTermine(){
		
	}

	@Override
	public void envoyerTicket(boolean premierTicket) {
		throw new IllegalStateException("Le client a fini de manger!");	
	}

	@Override
	public void creationTerminee() {
		throw new IllegalStateException("Le client a fini de manger!");	
	}
}
