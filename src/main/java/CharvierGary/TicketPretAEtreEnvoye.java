package CharvierGary;

public class TicketPretAEtreEnvoye implements EtatTicket {
    private Ticket ticket;
    
    public TicketPretAEtreEnvoye(Ticket ticket){
    	this.ticket = ticket;
    }

	
	public void annoterTicket() {
		ticket.setEtat(new TicketEnvoye(ticket));	
	}

}
