package CharvierGary;

import java.util.ArrayList;
import java.util.List;

public class Ticket {
    private List<ElementMenu> elementMenu = new ArrayList<ElementMenu> ();
    
    private EtatTicket etat = new TicketPretAEtreEnvoye(this);
    


    public Ticket(List<ElementMenu> listeElementsTicket){
    	this.setElementMenu(listeElementsTicket);
    }
    
    
    public void annoterTicket(){
    	this.etat.annoterTicket();
    }
    
    public List<ElementMenu> afficherComposition()
    {
    	this.etat.annoterTicket();
    	return this.elementMenu;
    }
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */

	public EtatTicket getEtat() {
		return etat;
	}

	public void setEtat(EtatTicket etat) {
		this.etat = etat;
	}

	public List<ElementMenu> getElementMenu() {
		return elementMenu;
	}

	public void setElementMenu(List<ElementMenu> elementMenu) {
		this.elementMenu = elementMenu;
	}
	
	
}
