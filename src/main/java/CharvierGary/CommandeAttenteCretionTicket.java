package CharvierGary;

import java.util.ArrayList;
import java.util.List;


public class CommandeAttenteCretionTicket implements EtatCommande {
	private Commande commande;
	
	public CommandeAttenteCretionTicket(Commande commande){
		this.commande = commande;
	}

	public void ajouterElement(List<ElementMenu> element) {
		throw new IllegalStateException("Vous ne pouvez pas ajouter d'éléments," +
				" vous pouvez seulement couper la commande en ticket");
	}

	public void creerTicket(List<ElementMenu> listeElementsTicket) {
		this.commande.getTickets().add(new Ticket(listeElementsTicket));
	}
	
	public void creationTerminee(){
		this.commande.setEtat(new CommandeAttentePreparation(this.commande));
		// On envoie le premier ticket dès que tous les tickets sont créés
		
	}

	public List<MenuDeCommande> recupMenus() {
		throw new IllegalStateException("Vous ne pouvez pas ajouter d'éléments," +
				" vous pouvez seulement couper la commande en ticket");
	}

	public List<ElementMenu> elementsRestants() {
    	List<ElementMenu> temp = new ArrayList<ElementMenu> ();
    	temp.addAll(commande.getElementMenu());
    	for(MenuDeCommande M : this.commande.getMenu()){
    		temp.addAll(M.retourneLesElements());
    	}
    	return temp;
	}

	public void ajouterMenu(List<MenuDeCarte> menu) {
		throw new IllegalStateException("Attente de création, pas possible d'ajouter !");
	}

	public void repasFini() {
		throw new IllegalStateException("Le repas est pas fini, il n'a même pas commencé..");
		
	}

	public float montantCommande() {
		throw new IllegalStateException("Le client ne va pas payer, il a pas mangé !");
	}

	public Commande commandeReglee() {
		throw new IllegalStateException("Le client n'a pas mangé, il n'a pas encore payé ..");		
	}
	
	public void choixTermine(){
		this.commande.setEtat(new CommandeAttentePreparation(commande));
	}

	@Override
	public void envoyerTicket(boolean premierTicket) {
		throw new IllegalStateException("Creer dans un premier temps les tickets");	
		
	}
}
