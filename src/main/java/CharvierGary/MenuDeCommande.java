package CharvierGary;

import java.util.ArrayList;
import java.util.List;

public class MenuDeCommande extends Menu {
    private MenuDeCarte menuDeCarte;

    private Entree entree = null;

    private Plat plat = null;

    private Dessert dessert = null;

    private Boisson boisson = null;
    
    public MenuDeCommande(MenuDeCarte menuDeCarte){
    	super(menuDeCarte.getNom(),menuDeCarte.getPrix(),menuDeCarte.getIdMenu());
    	this.setMenuDeCarte(menuDeCarte);
    	this.initialisation();
    }
    
    private void initialisation(){
    	// Pour chaque liste qui ne contient qu'un élément, on met automatiquement cet unique élément
    	if (getMenuDeCarte().getBoisson().size() == 1) this.setBoisson(getMenuDeCarte().getBoisson().get(0));
    	if (getMenuDeCarte().getDessert().size() == 1) this.setDessert(getMenuDeCarte().getDessert().get(0));
    	if (getMenuDeCarte().getPlat().size() == 1) this.setPlat(getMenuDeCarte().getPlat().get(0));
    	if (getMenuDeCarte().getEntree().size() == 1) this.setEntree(getMenuDeCarte().getEntree().get(0));
    	
    }
    
    public List<ElementMenu> retourneLesElements(){
    	List<ElementMenu> temp = new ArrayList<ElementMenu>();
    	if (getEntree() != null) temp.add(getEntree());
    	if (getPlat() != null) temp.add(getPlat());
    	if (getDessert() != null) temp.add(getDessert());
    	if (getBoisson() != null) temp.add(getBoisson());
    	return temp;
    }
    
    public List<ElementMenu> recupSelection (String type){
    		return this.menuDeCarte.recupSelection(type);
    }

    public void choisirElement(ElementMenu elementChoisi) {
    	if (elementChoisi instanceof Boisson) this.setBoisson((Boisson) elementChoisi);
    	if (elementChoisi instanceof Entree) this.setEntree((Entree) elementChoisi);
    	if (elementChoisi instanceof Plat) this.setPlat((Plat) elementChoisi);
    	if (elementChoisi instanceof Dessert) this.setDessert((Dessert) elementChoisi);
    }

	public MenuDeCarte getMenuDeCarte() {
		return menuDeCarte;
	}
	
	
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */

	public void setMenuDeCarte(MenuDeCarte menuDeCarte) {
		this.menuDeCarte = menuDeCarte;
	}

	public Entree getEntree() {
		return entree;
	}

	public void setEntree(Entree entree) {
		this.entree = entree;
	}

	public Plat getPlat() {
		return plat;
	}

	public void setPlat(Plat plat) {
		this.plat = plat;
	}

	public Dessert getDessert() {
		return dessert;
	}

	public void setDessert(Dessert dessert) {
		this.dessert = dessert;
	}

	public Boisson getBoisson() {
		return boisson;
	}

	public void setBoisson(Boisson boisson) {
		this.boisson = boisson;
	}

}
