package CharvierGary;

public class ElementMenu {
    private String Nom;

    private float Prix;
    
    public ElementMenu(String nom, float prix){
    	this.Nom = nom;
    	this.Prix = prix;
    }
    
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */

    float getPrix() {
        return this.Prix;
    }

    void setPrix(float value) {
        this.Prix = value;
    }

    String getNom() {
        return this.Nom;
    }

    void setNom(String value) {
        this.Nom = value;
    }

}
