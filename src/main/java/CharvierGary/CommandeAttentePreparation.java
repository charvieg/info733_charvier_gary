package CharvierGary;

import java.util.List;


public class CommandeAttentePreparation implements EtatCommande {
	private Commande commande;
	
	public CommandeAttentePreparation(Commande commande){
		this.commande = commande;
	}

	@Override
	public void ajouterElement(List<ElementMenu> element) {
		throw new IllegalStateException("Commande en attente de préparation!");	
		
	}

	@Override
	public void creerTicket(List<ElementMenu> listeElementsTicket) {
		throw new IllegalStateException("Commande en attente de préparation!");			
	}

	@Override
	public List<MenuDeCommande> recupMenus() {
		throw new IllegalStateException("Commande en attente de préparation!");	
	}

	@Override
	public List<ElementMenu> elementsRestants() {
		throw new IllegalStateException("Commande en attente de préparation!");	
	}

	@Override
	public void ajouterMenu(List<MenuDeCarte> menu) {
		throw new IllegalStateException("Commande en attente de préparation!");	
		
	}

	@Override
	public void repasFini() {
		this.commande.setEtat(new CommandeAttentePaiement(this.commande));
	}

	@Override
	public float montantCommande() {
		throw new IllegalStateException("Commande en attente de préparation!");	
	}

	@Override
	public Commande commandeReglee() {
		throw new IllegalStateException("Commande en attente de préparation!");	
	}
	
	public void choixTermine(){
		throw new IllegalStateException("Commande en attente de préparation!");	
	}
	
	public void envoyerTicket(boolean premierTicket){
		if(!premierTicket){
			this.commande.getTickets().remove(0);
		}
		
		if(this.commande.resteTicket())
		{
			this.commande.getTickets().get(0).annoterTicket();
		}
		if (!this.commande.resteTicket())
		{
			this.commande.repasFini();
		}
	}

	@Override
	public void creationTerminee() {
		throw new IllegalStateException("Commande en attente de préparation!");	
	}
}
