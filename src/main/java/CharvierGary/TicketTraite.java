package CharvierGary;

public class TicketTraite implements EtatTicket {
	Ticket ticket;
	
	public TicketTraite(Ticket ticket){
		this.ticket = ticket;
	}
	@Override
	public void annoterTicket() {
		throw new IllegalStateException("Le ticket a été servi !");	
	}
	
	public String toString(){
		return "TickeTraite";
	}
}
