package CharvierGary;

import java.util.ArrayList;
import java.util.List;

public class ListeElementMenu {
    private List<Boisson> listeBoissons = new ArrayList<Boisson> ();

    private List<Dessert> listeDesserts = new ArrayList<Dessert> ();

    private List<Plat> listePlats = new ArrayList<Plat> ();

    private List<Entree> listeEntrees = new ArrayList<Entree> ();

    public void getListe(String type) {
    }
    
    
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */

	public List<Boisson> getListeBoissons() {
		return listeBoissons;
	}

	public void setListeBoissons(List<Boisson> listeBoissons) {
		this.listeBoissons = listeBoissons;
	}

	public List<Dessert> getListeDesserts() {
		return listeDesserts;
	}

	public void setListeDesserts(List<Dessert> listeDesserts) {
		this.listeDesserts = listeDesserts;
	}

	public List<Plat> getListePlats() {
		return listePlats;
	}

	public void setListePlats(List<Plat> listePlats) {
		this.listePlats = listePlats;
	}

	public List<Entree> getListeEntrees() {
		return listeEntrees;
	}

	public void setListeEntrées(List<Entree> listeEntrees) {
		this.listeEntrees = listeEntrees;
	}

}
