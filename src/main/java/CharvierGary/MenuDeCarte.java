package CharvierGary;

import java.util.ArrayList;
import java.util.List;

public class MenuDeCarte extends Menu {

    private List<Dessert> dessert = new ArrayList<Dessert> ();

    private List<Boisson> boisson = new ArrayList<Boisson> ();

    private List<Plat> plat = new ArrayList<Plat> ();

    private List<Entree> entrée = new ArrayList<Entree> ();

    public MenuDeCarte(int idMenu, String nom, float prix){
    	super(nom, prix, idMenu);
    }


	public boolean esTuMenu(int idMenu){
    	if (idMenu == this.getIdMenu()) return true;
    	else return false;
    }
    
    public List<ElementMenu> recupSelection(String type) {
    List<ElementMenu> LM = new ArrayList<ElementMenu>();
    
		switch (type){
		case "boisson":
			LM.addAll(this.getBoisson());
			return LM;
		case "entree":
			LM.addAll(this.getEntree());
			return LM;
		case "plat":
			LM.addAll(this.getPlat());
			return LM;
		case "dessert":
			LM.addAll(this.getDessert());
			return LM;
		}
		return LM;
    }

    
	
	/* 
	 * ----------------------------------------------
	 * ------------ GETTERS AND SETTERS -------------
	 * ----------------------------------------------
	 */
    
	public List<Dessert> getDessert() {
		return dessert;
	}

	public void setDessert(List<Dessert> dessert) {
		this.dessert = dessert;
	}

	public List<Boisson> getBoisson() {
		return boisson;
	}

	public void setBoisson(List<Boisson> boisson) {
		this.boisson = boisson;
	}

	public List<Plat> getPlat() {
		return plat;
	}

	public void setPlat(List<Plat> plat) {
		this.plat = plat;
	}

	public List<Entree> getEntree() {
		return entrée;
	}

	public void setEntree(List<Entree> entrée) {
		this.entrée = entrée;
	}


}
