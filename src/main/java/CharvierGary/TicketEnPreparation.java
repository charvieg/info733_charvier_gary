package CharvierGary;

public class TicketEnPreparation implements EtatTicket {
	Ticket ticket;
	
	public TicketEnPreparation(Ticket ticket){
		this.ticket = ticket;
	}
	@Override
	public void annoterTicket() {
		ticket.setEtat(new TicketTraite(ticket));
	}

}
