package CharvierGary;

import org.junit.Assert;
import org.junit.Test;

import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class TicketTest
{

    @Test
    public void stateOfANewTicketIsPretAEtreEnvoye(){
        List <ElementMenu> L = new ArrayList<ElementMenu>();
        Assert.assertEquals(new TicketPretAEtreEnvoye(new Ticket(L)).getClass().toString(),
                new Ticket(L).getEtat().getClass().toString());
    }


    // Ce test est validé si on lève une IllegalStateException
    @Test
    public void couldntPrintCompositionOfATicketEnPreparation () throws IllegalStateException{
        List <ElementMenu> L = new ArrayList<ElementMenu>();
        Ticket T = new Ticket(L);
        T.setEtat(new TicketEnPreparation(T));
        T.afficherComposition();
    }

}
